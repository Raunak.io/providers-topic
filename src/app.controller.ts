import { TestService } from './test.service';
import { Controller, Get ,Body,Post,Param} from '@nestjs/common';
import { AppService } from './app.service';

class TestDto{
  name:string
}


@Controller()
export class AppController {
  constructor(private readonly appService: AppService,private readonly testService:TestService) {}

  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
  
  @Get(':id')
 async  getId(@Param('id') userId): Promise<any>{
    return this.testService.getHelloAgain();
  }
  @Post()
 async  create(@Body() data:TestDto): Promise<any>{
   await this.testService.create(data);
    return 'created';
  }
 
}
