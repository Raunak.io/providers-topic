import { Injectable } from '@nestjs/common';
 interface Test{
    name:string
}
@Injectable()
export class TestService {
 private readonly Tests: Test[] = [];

//can also use @Inject() property based injection but  constructor based is preffered most


  getHelloAgain(): string {
    return 'Hello again';
  }

  async create(Test: Test) :Promise<string>{
    await this.Tests.push(Test);
    return 'created';
  }

  findAll(): Test[] {
    return this.Tests;
  }
}
